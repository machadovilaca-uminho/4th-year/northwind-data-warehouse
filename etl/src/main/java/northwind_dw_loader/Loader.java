package northwind_dw_loader;

import northwind_entities_save.DWEntity;
import org.reflections.Reflections;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Loader {
    private Map<String, List<String>> dumpData;

    public Loader() {
        this.dumpData = new HashMap<>();
    }

    public Map<String, List<String>> getDumpData() {
        return dumpData;
    }

    public void loadAllFrom(String filenamePrefix, String suffixPrefix) {
        Reflections reflections = new Reflections("northwind_entities");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(DWEntity.class);

        annotated.forEach(c -> loadOneFrom(c, filenamePrefix + c.getSimpleName() + suffixPrefix));
    }

    private void loadOneFrom(Class<?> c, String filename) {
        try {
            List<String> list = Files.readAllLines(new File(filename).toPath());
            dumpData.put(c.getSimpleName(), list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

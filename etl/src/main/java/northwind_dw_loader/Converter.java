package northwind_dw_loader;

import northwind_dw_entities.*;
import northwind_entities.*;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Converter {
    private static List<String> holidays = Arrays.asList("01 01", "01 21", "02 18", "05 12", "05 27", "06 16", "07 04", "09 02", "10 14", "11 11", "11 28", "11 29", "12 25");

    private Map<String, List<String>> dumpData;

    private List<CustomerDimension> customers;
    private List<DateDimension> dates;
    private List<EmployeeDimension> employees;
    private List<OrderFacts> orders;
    private List<PaymentMethodDimension> paymentMethods;
    private List<ProductDimension> products;
    private List<ShipperDimension> shippers;

    public Converter(Map<String, List<String>> dumpData) {
        this.dumpData = dumpData;
        this.customers = new ArrayList<>();
        this.dates = new ArrayList<>();
        this.employees = new ArrayList<>();
        this.orders = new ArrayList<>();
        this.paymentMethods = new ArrayList<>();
        this.products = new ArrayList<>();
        this.shippers = new ArrayList<>();
    }

    public List<CustomerDimension> getCustomers() {
        return customers;
    }

    public List<DateDimension> getDates() {
        return dates;
    }

    public List<EmployeeDimension> getEmployees() {
        return employees;
    }

    public List<OrderFacts> getOrders() {
        return orders;
    }

    public List<PaymentMethodDimension> getPaymentMethods() {
        return paymentMethods;
    }

    public List<ProductDimension> getProducts() {
        return products;
    }

    public List<ShipperDimension> getShippers() {
        return shippers;
    }

    public List<OrderFacts> loadOrderFacts() {
        List<OrderFacts> orderFacts = new ArrayList<>();
        List<String> rawOrders =  dumpData.get(Orders.class.getSimpleName());
        AtomicInteger i = new AtomicInteger();

        rawOrders.forEach(e -> {
            String[] order = e.substring(1, e.length() - 1).split("\",\"");
            orderFacts.addAll(orderDetailsWhereOrder(order, i));
       });

        return orderFacts;
    }

    public void loadDimensions() {
        loadEmployees();
        loadCustomers();
        loadShippers();
        loadPaymentMethod();
        loadProducts();
        loadDates();
    }

    private void loadDates() {
        AtomicInteger i = new AtomicInteger();
        LocalDate now = LocalDate.now();
        DateTimeFormatter fullDescription = DateTimeFormat.forPattern("MMMM d, y");
        DateTimeFormatter dayOfTheWeek = DateTimeFormat.forPattern("EEEE");
        DateTimeFormatter month = DateTimeFormat.forPattern("MMMM");

        for (
                LocalDate localDate = new LocalDate("2005-01-01");
                localDate.isBefore(now) || localDate.isEqual(now);
                localDate = localDate.plusDays(1)
        ) {
            DateDimension date = new DateDimension();
            date.setDateId(i.getAndIncrement());
            date.setDate(localDate.toString());
            date.setFullDateDescription(localDate.toString(fullDescription));
            date.setDayOfTheWeek(localDate.toString(dayOfTheWeek));
            date.setCalendarMonth(localDate.toString(month));
            date.setCalendarQuarter("Q" + ((localDate.getMonthOfYear() / 4) + 1));
            date.setCalendarYear(Integer.toString(localDate.getYear()));
            date.setHolidayIndicator(Byte.parseByte(isHoliday(localDate) ? "1" : "0"));
            date.setWeekdayIndicator(Byte.parseByte(localDate.getDayOfWeek() < 6 ? "1" : "0"));

            dates.add(date);
        }
    }

    private boolean isHoliday(LocalDate localDate) {
        return holidays.contains(localDate.getMonthOfYear() + " " + localDate.getDayOfMonth());
    }

    private void loadProducts() {
        List<String> rawProducts = dumpData.get(Products.class.getSimpleName());
        rawProducts.forEach(e -> {
            String[] data = e.substring(1, e.length() - 1).split("\",\"");
            ProductDimension product = new ProductDimension();
            product.setProductId(Integer.parseInt(data[0]));
            product.setProductCode(data[1]);
            product.setProductName(data[2]);
            product.setDescription(data[3].equals("null") ? null : data[3]);
            product.setQuantityPerUnit(data[6].equals("null") ? null : data[6]);
            product.setDiscontinued(data[7].equals("true") ? Byte.parseByte("1") : Byte.parseByte("0"));
            product.setCategory(data[8]);
            products.add(product);
        });
    }

    private void loadPaymentMethod() {
        List<String> rawOrders = dumpData.get(Orders.class.getSimpleName());
        AtomicInteger i = new AtomicInteger();
        rawOrders.forEach(e -> {
            String[] data = e.substring(1, e.length() - 1).split("\",\"");
            PaymentMethodDimension paymentMethod = new PaymentMethodDimension();
            paymentMethod.setPaymentMethodId(i.getAndIncrement());
            paymentMethod.setPaymentType(data[12].equals("null") ? null : data[12]);

            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                Date parsedDate = dateFormat.parse(data[13]);
                Timestamp timestamp = new Timestamp(parsedDate.getTime());
                paymentMethod.setPaidDate(timestamp);
            } catch (Exception ex) {
                paymentMethod.setPaidDate(null);
            }

            paymentMethods.add(paymentMethod);
        });
    }

    private void loadEmployees() {
        List<String> rawEmployees = dumpData.get(Employees.class.getSimpleName());
        rawEmployees.forEach(e -> {
            String[] data = e.substring(1, e.length() - 1).split("\",\"");
            EmployeeDimension employee = new EmployeeDimension();
            employee.setEmployeeId(Integer.parseInt(data[0]));
            employee.setPrivilegeName(data[1].replaceAll("([\\[\\]])", ""));
            employee.setCompany(data[2]);
            employee.setLastName(data[3]);
            employee.setFirstName(data[4]);
            employee.setEmailAddress(data[5]);
            employee.setJobTitle(data[6]);
            employee.setBusinessPhone(data[7]);
            employee.setHomePhone(data[8]);
            employee.setMobilePhone(data[9]);
            employee.setFaxNumber(data[10]);
            employee.setAddress(data[11]);
            employee.setCity(data[12]);
            employee.setStateProvince(data[13]);
            employee.setZipPostalCode(data[14]);
            employee.setCountryRegion(data[15]);
            employee.setWebPage(data[16]);
            employee.setNotes(data[17].equals("null") ? null : data[17]);
            employee.setActive(Byte.parseByte("1"));
            employees.add(employee);
        });
    }

    private void loadCustomers() {
        List<String> rawCustomers = dumpData.get(Customers.class.getSimpleName());
        rawCustomers.forEach(e -> {
            String[] data = e.substring(1,e.length()-1).split("\",\"");
            CustomerDimension customer = new CustomerDimension();
            customer.setCustomerId(Integer.parseInt(data[0]));
            customer.setCompany(data[1]);
            customer.setLastName(data[2]);
            customer.setFirstName(data[3]);
            customer.setEmailAddress(data[4]);
            customer.setJobTitle(data[5]);
            customer.setBusinessPhone(data[6]);
            customer.setHomePhone(data[7]);
            customer.setMobilePhone(data[8]);
            customer.setFaxNumber(data[9]);
            customer.setAddress(data[10]);
            customer.setCity(data[11]);
            customer.setStateProvince(data[12]);
            customer.setZipPostalCode(data[13]);
            customer.setCountryRegion(data[14]);
            customer.setWebPage(data[15]);
            customer.setNotes(data[16].equals("null") ? null : data[17]);
            List<String> orderShip = lastOrderWhereCustomerId(data[0]);
            customer.setShipAddress(orderShip.get(0));
            customer.setShipCity(orderShip.get(1));
            customer.setStateProvince(orderShip.get(2));
            customer.setZipPostalCode(orderShip.get(3));
            customer.setCountryRegion(orderShip.get(4));
            customer.setActive(Byte.parseByte("1"));
            customers.add(customer);
        });
    }

    private void loadShippers() {
        List<String> rawShippers = dumpData.get(Shippers.class.getSimpleName());
        rawShippers.forEach(e -> {
            String[] data = e.substring(1,e.length()-1).split("\",\"");
            ShipperDimension shipper = new ShipperDimension();
            shipper.setShipperId(Integer.parseInt(data[0]));
            shipper.setCompany(data[1]);
            shipper.setLastName(data[2]);
            shipper.setFirstName(data[3]);
            shipper.setEmailAddress(data[4]);
            shipper.setJobTitle(data[5]);
            shipper.setBusinessPhone(data[6]);
            shipper.setHomePhone(data[7]);
            shipper.setMobilePhone(data[8]);
            shipper.setFaxNumber(data[9]);
            shipper.setAddress(data[10]);
            shipper.setCity(data[11]);
            shipper.setStateProvince(data[12]);
            shipper.setZipPostalCode(data[13]);
            shipper.setCountryRegion(data[14]);
            shipper.setWebPage(data[15]);
            shipper.setNotes(data[16].equals("null") ? null : data[17]);
            shipper.setActive(Byte.parseByte("1"));
            shippers.add(shipper);
        });
    }

    private List<String> lastOrderWhereCustomerId(String id) {
        List<String> results = new ArrayList<>();

        List<String> rawOrders = dumpData.get(Orders.class.getSimpleName());
        String order = rawOrders.stream()
                .filter(r -> r.substring(1,r.length()-1).split("\",\"")[3].equals(id))
                .findFirst().orElse("");

        if(order.equals("")) {
            return Arrays.asList("", "", "", "", "");
        }

        String[] data = order.substring(1,order.length()-1).split("\",\"");
        results.add(data[5]);
        results.add(data[6]);
        results.add(data[7]);
        results.add(data[8]);
        results.add(data[9]);

        return results;
    }

    private List<OrderFacts> orderDetailsWhereOrder(String[] order, AtomicInteger i) {
        List<OrderFacts> orderFacts = new ArrayList<>();

        List<String> rawOrderDetails = dumpData.get(OrderDetails.class.getSimpleName());
        rawOrderDetails.stream()
                .filter(r -> r.substring(1,r.length()-1).split("\",\"")[1].equals(order[0]))
                .forEach(e -> {
                    String[] orderDetails = e.substring(1, e.length() - 1).split("\",\"");
                    String[] product = getProductWhereId(orderDetails[2]);

                    OrderFacts o = new OrderFacts();
                    o.setOrderId(i.getAndIncrement());
                    o.setShipperDimensionShipperId(order[1].equals("null") ? null : Integer.parseInt(order[1]));
                    o.setCustomerDimensionCustomerId(Integer.parseInt(order[2]));
                    o.setEmployeeDimensionEmployeeId(Integer.parseInt(order[3]));
                    o.setProductDimensionProductId(Integer.parseInt(orderDetails[2]));
                    o.setDateDimensionDateId(getDate(order[4]));
                    o.setPaymentMethodDimensionPaymentMethodId(getPaymentMethodId(order));
                    o.setUnitPrice(new BigDecimal(orderDetails[4]));
                    o.setDiscount(Double.parseDouble(orderDetails[5]));
                    o.setListPrice((new BigDecimal(product[5])));
                    o.setStandardCost(new BigDecimal(product[4]));
                    o.setQuantity(new BigDecimal(orderDetails[3]));
                    o.setExtendedSalesAmount(o.getQuantity().multiply(o.getUnitPrice()));
                    o.setExtendedCostAmount(getCost(orderDetails[2]).multiply(o.getQuantity()));
                    o.setExtendedProfitAmount(o.getExtendedSalesAmount().subtract(o.getExtendedCostAmount()));
                    o.setShippingFee(new BigDecimal(order[10]));
                    o.setTaxes(new BigDecimal(order[11]));
                    o.setTaxRate(Double.parseDouble(order[14]));

                    orderFacts.add(o);
                });

        return orderFacts;
    }

    private Integer getPaymentMethodId(String[] order) {
        String type = order[12].equals("null") ? null : order[12];

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

        Timestamp timestamp = null;
        try {
            Date parsedDate = dateFormat.parse(order[13]);
            timestamp = new Timestamp(parsedDate.getTime());
        } catch (ParseException ignored) {}

        if(type == null || timestamp == null) {
            PaymentMethodDimension paymentMethod = paymentMethods
                    .stream()
                    .filter(p -> (p.getPaidDate() == null && p.getPaymentType() == null))
                    .findFirst().orElse(null);

            return paymentMethod != null ? paymentMethod.getPaymentMethodId() : null;
        }

        Timestamp finalTimestamp = timestamp;
        PaymentMethodDimension paymentMethod = paymentMethods
                .stream()
                .filter(p -> (
                        p.getPaidDate() != null &&
                        p.getPaymentType() != null &&
                        p.getPaidDate().equals(finalTimestamp) &&
                        p.getPaymentType().equals(type))
                )
                .findFirst().orElse(null);

        return paymentMethod != null ? paymentMethod.getPaymentMethodId() : null;
    }

    private String[] getProductWhereId(String id) {
        List<String> rawProducts = dumpData.get(Products.class.getSimpleName());
        String product = rawProducts.stream()
                .filter(r -> r.substring(1,r.length()-1).split("\",\"")[0].equals(id))
                .findFirst().orElse("");

        return product.substring(1,product.length()-1).split("\",\"");
    }

    private Integer getDate(String date) {
        DateDimension d = dates.stream()
                .filter(r -> date.substring(0, 10).equals(r.getDate()))
                .findFirst().orElse(null);

        assert d != null;
        return d.getDateId();
    }

    private BigDecimal getCost(String productId) {
        List<String> purchases = dumpData.get(PurchaseOrderDetails.class.getSimpleName());

        System.out.println(purchases.stream()
                .filter(r -> r.substring(1,r.length()-1).split("\",\"")[2].equals(productId))
                .collect(Collectors.toList())
        );

        List<String> ps = purchases.stream()
                .filter(r -> r.substring(1,r.length()-1).split("\",\"")[2].equals(productId))
                .collect(Collectors.toList());

        String p = ps.get(ps.size()-1);

        return new BigDecimal(p.substring(1, p.length()-1).split("\",\"")[4]);
    }
}

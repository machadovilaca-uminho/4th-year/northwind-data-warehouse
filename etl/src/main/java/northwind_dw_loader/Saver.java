package northwind_dw_loader;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;

import javax.persistence.Entity;
import java.util.List;
import java.util.Set;

public class Saver {
    private SessionFactory sessionFactory;

    public Saver(String configurationFile, String packageName) {
        Configuration cfg = new Configuration().configure(configurationFile);
        Reflections reflections = new Reflections(packageName);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Entity.class);

        annotated.forEach(cfg::addAnnotatedClass);

        sessionFactory = cfg.buildSessionFactory();
    }

    public void dumpData(List<?> data) {
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        data.forEach(session::save);

        session.getTransaction().commit();
        session.close();
    }
}

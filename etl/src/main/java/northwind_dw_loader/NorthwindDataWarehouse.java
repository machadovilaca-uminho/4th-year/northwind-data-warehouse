package northwind_dw_loader;

public class NorthwindDataWarehouse {
    public static void main(String[] args) {
        Loader loader = new Loader();
        loader.loadAllFrom("/tmp/", "_tmp");
        Converter converter = new Converter(loader.getDumpData());
        converter.loadDimensions();

        Saver saver = new Saver("/northwind_dw.cfg.xml", "northwind_dw_entities");
        saver.dumpData(converter.getEmployees());
        saver.dumpData(converter.getCustomers());
        saver.dumpData(converter.getShippers());
        saver.dumpData(converter.getPaymentMethods());
        saver.dumpData(converter.getProducts());
        saver.dumpData(converter.getDates());

        saver.dumpData(converter.loadOrderFacts());
    }
}

package northwind_dw_entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "payment_method_dimension", schema = "northwind_dw", catalog = "")
public class PaymentMethodDimension {
    private int paymentMethodId;
    private String paymentType;
    private Timestamp paidDate;

    @Id
    @Column(name = "payment_method_id")
    public int getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(int paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    @Basic
    @Column(name = "payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @Basic
    @Column(name = "paid_date")
    public Timestamp getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Timestamp paidDate) {
        this.paidDate = paidDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentMethodDimension that = (PaymentMethodDimension) o;
        return paymentMethodId == that.paymentMethodId &&
                Objects.equals(paymentType, that.paymentType) &&
                Objects.equals(paidDate, that.paidDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(paymentMethodId, paymentType, paidDate);
    }
}

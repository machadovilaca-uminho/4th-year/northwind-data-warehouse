package northwind_dw_entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "date_dimension", schema = "northwind_dw", catalog = "")
public class DateDimension {
    private int dateId;
    private String date;
    private String fullDateDescription;
    private String dayOfTheWeek;
    private String calendarMonth;
    private String calendarQuarter;
    private String calendarYear;
    private Byte holidayIndicator;
    private Byte weekdayIndicator;

    @Id
    @Column(name = "date_id")
    public int getDateId() {
        return dateId;
    }

    public void setDateId(int dateId) {
        this.dateId = dateId;
    }

    @Basic
    @Column(name = "date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Basic
    @Column(name = "full_date_description")
    public String getFullDateDescription() {
        return fullDateDescription;
    }

    public void setFullDateDescription(String fullDateDescription) {
        this.fullDateDescription = fullDateDescription;
    }

    @Basic
    @Column(name = "day_of_the_week")
    public String getDayOfTheWeek() {
        return dayOfTheWeek;
    }

    public void setDayOfTheWeek(String dayOfTheWeek) {
        this.dayOfTheWeek = dayOfTheWeek;
    }

    @Basic
    @Column(name = "calendar_month")
    public String getCalendarMonth() {
        return calendarMonth;
    }

    public void setCalendarMonth(String calendarMonth) {
        this.calendarMonth = calendarMonth;
    }

    @Basic
    @Column(name = "calendar_quarter")
    public String getCalendarQuarter() {
        return calendarQuarter;
    }

    public void setCalendarQuarter(String calendarQuarter) {
        this.calendarQuarter = calendarQuarter;
    }

    @Basic
    @Column(name = "calendar_year")
    public String getCalendarYear() {
        return calendarYear;
    }

    public void setCalendarYear(String calendarYear) {
        this.calendarYear = calendarYear;
    }

    @Basic
    @Column(name = "holiday_indicator")
    public Byte getHolidayIndicator() {
        return holidayIndicator;
    }

    public void setHolidayIndicator(Byte holidayIndicator) {
        this.holidayIndicator = holidayIndicator;
    }

    @Basic
    @Column(name = "weekday_indicator")
    public Byte getWeekdayIndicator() {
        return weekdayIndicator;
    }

    public void setWeekdayIndicator(Byte weekdayIndicator) {
        this.weekdayIndicator = weekdayIndicator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateDimension that = (DateDimension) o;
        return dateId == that.dateId &&
                Objects.equals(date, that.date) &&
                Objects.equals(fullDateDescription, that.fullDateDescription) &&
                Objects.equals(dayOfTheWeek, that.dayOfTheWeek) &&
                Objects.equals(calendarMonth, that.calendarMonth) &&
                Objects.equals(calendarQuarter, that.calendarQuarter) &&
                Objects.equals(calendarYear, that.calendarYear) &&
                Objects.equals(holidayIndicator, that.holidayIndicator) &&
                Objects.equals(weekdayIndicator, that.weekdayIndicator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateId, date, fullDateDescription, dayOfTheWeek, calendarMonth, calendarQuarter, calendarYear, holidayIndicator, weekdayIndicator);
    }
}

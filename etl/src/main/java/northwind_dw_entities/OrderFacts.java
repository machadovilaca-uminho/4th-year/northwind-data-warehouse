package northwind_dw_entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "order_facts", schema = "northwind_dw", catalog = "")
public class OrderFacts {
    private int orderId;
    private Integer dateDimensionDateId;
    private Integer productDimensionProductId;
    private Integer customerDimensionCustomerId;
    private Integer employeeDimensionEmployeeId;
    private Integer shipperDimensionShipperId;
    private Integer paymentMethodDimensionPaymentMethodId;
    private Double taxRate;
    private BigDecimal quantity;
    private Double discount;
    private BigDecimal unitPrice;
    private BigDecimal shippingFee;
    private BigDecimal taxes;
    private BigDecimal listPrice;
    private BigDecimal standardCost;
    private BigDecimal extendedSalesAmount;
    private BigDecimal extendedCostAmount;
    private BigDecimal extendedProfitAmount;

    @Id
    @Column(name = "order_id")
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "date_dimension_date_id")
    public Integer getDateDimensionDateId() {
        return dateDimensionDateId;
    }

    public void setDateDimensionDateId(Integer dateDimensionDateId) {
        this.dateDimensionDateId = dateDimensionDateId;
    }

    @Basic
    @Column(name = "product_dimension_product_id")
    public Integer getProductDimensionProductId() {
        return productDimensionProductId;
    }

    public void setProductDimensionProductId(Integer productDimensionProductId) {
        this.productDimensionProductId = productDimensionProductId;
    }

    @Basic
    @Column(name = "customer_dimension_customer_id")
    public Integer getCustomerDimensionCustomerId() {
        return customerDimensionCustomerId;
    }

    public void setCustomerDimensionCustomerId(Integer customerDimensionCustomerId) {
        this.customerDimensionCustomerId = customerDimensionCustomerId;
    }

    @Basic
    @Column(name = "employee_dimension_employee_id")
    public Integer getEmployeeDimensionEmployeeId() {
        return employeeDimensionEmployeeId;
    }

    public void setEmployeeDimensionEmployeeId(Integer employeeDimensionEmployeeId) {
        this.employeeDimensionEmployeeId = employeeDimensionEmployeeId;
    }

    @Basic
    @Column(name = "shipper_dimension_shipper_id")
    public Integer getShipperDimensionShipperId() {
        return shipperDimensionShipperId;
    }

    public void setShipperDimensionShipperId(Integer shipperDimensionShipperId) {
        this.shipperDimensionShipperId = shipperDimensionShipperId;
    }

    @Basic
    @Column(name = "payment_method_dimension_payment_method_id")
    public Integer getPaymentMethodDimensionPaymentMethodId() {
        return paymentMethodDimensionPaymentMethodId;
    }

    public void setPaymentMethodDimensionPaymentMethodId(Integer paymentMethodDimensionPaymentMethodId) {
        this.paymentMethodDimensionPaymentMethodId = paymentMethodDimensionPaymentMethodId;
    }

    @Basic
    @Column(name = "tax_rate")
    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    @Basic
    @Column(name = "quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "discount")
    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    @Basic
    @Column(name = "unit_price")
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    @Basic
    @Column(name = "shipping_fee")
    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    @Basic
    @Column(name = "taxes")
    public BigDecimal getTaxes() {
        return taxes;
    }

    public void setTaxes(BigDecimal taxes) {
        this.taxes = taxes;
    }

    @Basic
    @Column(name = "list_price")
    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    @Basic
    @Column(name = "standard_cost")
    public BigDecimal getStandardCost() {
        return standardCost;
    }

    public void setStandardCost(BigDecimal standardCost) {
        this.standardCost = standardCost;
    }

    @Basic
    @Column(name = "extended_sales_amount")
    public BigDecimal getExtendedSalesAmount() {
        return extendedSalesAmount;
    }

    public void setExtendedSalesAmount(BigDecimal extendedSalesAmount) {
        this.extendedSalesAmount = extendedSalesAmount;
    }

    @Basic
    @Column(name = "extended_cost_amount")
    public BigDecimal getExtendedCostAmount() {
        return extendedCostAmount;
    }

    public void setExtendedCostAmount(BigDecimal extendedCostAmount) {
        this.extendedCostAmount = extendedCostAmount;
    }

    @Basic
    @Column(name = "extended_profit_amount")
    public BigDecimal getExtendedProfitAmount() {
        return extendedProfitAmount;
    }

    public void setExtendedProfitAmount(BigDecimal extendedProfitAmount) {
        this.extendedProfitAmount = extendedProfitAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderFacts that = (OrderFacts) o;
        return orderId == that.orderId &&
                Objects.equals(dateDimensionDateId, that.dateDimensionDateId) &&
                Objects.equals(productDimensionProductId, that.productDimensionProductId) &&
                Objects.equals(customerDimensionCustomerId, that.customerDimensionCustomerId) &&
                Objects.equals(employeeDimensionEmployeeId, that.employeeDimensionEmployeeId) &&
                Objects.equals(shipperDimensionShipperId, that.shipperDimensionShipperId) &&
                Objects.equals(paymentMethodDimensionPaymentMethodId, that.paymentMethodDimensionPaymentMethodId) &&
                Objects.equals(taxRate, that.taxRate) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(discount, that.discount) &&
                Objects.equals(unitPrice, that.unitPrice) &&
                Objects.equals(shippingFee, that.shippingFee) &&
                Objects.equals(taxes, that.taxes) &&
                Objects.equals(listPrice, that.listPrice) &&
                Objects.equals(standardCost, that.standardCost) &&
                Objects.equals(extendedSalesAmount, that.extendedSalesAmount) &&
                Objects.equals(extendedCostAmount, that.extendedCostAmount) &&
                Objects.equals(extendedProfitAmount, that.extendedProfitAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, dateDimensionDateId, productDimensionProductId, customerDimensionCustomerId, employeeDimensionEmployeeId, shipperDimensionShipperId, paymentMethodDimensionPaymentMethodId, taxRate, quantity, discount, unitPrice, shippingFee, taxes, listPrice, standardCost, extendedSalesAmount, extendedCostAmount, extendedProfitAmount);
    }
}

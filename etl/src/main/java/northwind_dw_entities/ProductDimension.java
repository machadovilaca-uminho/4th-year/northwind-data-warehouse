package northwind_dw_entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "product_dimension", schema = "northwind_dw", catalog = "")
public class ProductDimension {
    private int productId;
    private String productCode;
    private String productName;
    private String description;
    private String quantityPerUnit;
    private Byte discontinued;
    private String category;

    @Id
    @Column(name = "product_id")
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "product_code")
    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    @Basic
    @Column(name = "product_name")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "quantity_per_unit")
    public String getQuantityPerUnit() {
        return quantityPerUnit;
    }

    public void setQuantityPerUnit(String quantityPerUnit) {
        this.quantityPerUnit = quantityPerUnit;
    }

    @Basic
    @Column(name = "discontinued")
    public Byte getDiscontinued() {
        return discontinued;
    }

    public void setDiscontinued(Byte discontinued) {
        this.discontinued = discontinued;
    }

    @Basic
    @Column(name = "category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDimension that = (ProductDimension) o;
        return productId == that.productId &&
                Objects.equals(productCode, that.productCode) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(quantityPerUnit, that.quantityPerUnit) &&
                Objects.equals(discontinued, that.discontinued) &&
                Objects.equals(category, that.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productCode, productName, description, quantityPerUnit, discontinued, category);
    }
}

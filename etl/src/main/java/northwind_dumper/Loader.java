package northwind_dumper;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;

import javax.persistence.Entity;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Set;

public class Loader {
    private SessionFactory sessionFactory;

    public Loader(String configurationFile, String packageName) {
        Configuration cfg = new Configuration().configure(configurationFile);
        Reflections reflections = new Reflections(packageName);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Entity.class);

        annotated.forEach(cfg::addAnnotatedClass);

        sessionFactory = cfg.buildSessionFactory();
    }

    public <T> List<T> loadAllData(Class<T> type) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(type);
        criteria.from(type);
        List<T> r = session.createQuery(criteria).getResultList();
        session.close();
        return r;
    }
}

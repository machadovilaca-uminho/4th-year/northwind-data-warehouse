package northwind_dumper;

import northwind_entities_save.DWEntity;
import org.reflections.Reflections;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

public class Northwind {
    private Loader loader;

    public static void main(String[] args) {
        Northwind n = new Northwind();
        n.dumpAllTo("/tmp/", "_tmp");
    }

    public Northwind() {
        this.loader = new Loader("/northwind.cfg.xml", "northwind_entities");
    }

    public void dumpAllTo(String filenamePrefix, String suffixPrefix) {
        Reflections reflections = new Reflections("northwind_entities");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(DWEntity.class);

        annotated.forEach(c -> dumpOneTo(c, filenamePrefix + c.getSimpleName() + suffixPrefix));
    }

    private void dumpOneTo(Class<?> c, String filename) {
        List<?> raw = loader.loadAllData(c);
        print(raw, filename);
    }

    private void print(List<?> raw, String fileName) {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileName))) {
            raw.forEach(printWriter::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
